<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<div id="main-content" class="bg">



	


<div id="primary" class="mx_width">
    
    <div class="innerLeft fl">
		<div class="weddingpage" role="main">

		<header>
				<h1 class="page-title"><?php _e( 'Not Found', 'twentyfourteen' ); ?></h1>
			</header>

			<div>
				<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'twentyfourteen' ); ?></p>

				<?php get_search_form(); ?>
			</div>	

		</div><!-- #content -->
        </div>
        <?php
get_sidebar(); ?>

<div class="cl"></div>
	</div>
	

<?php get_sidebar(); get_footer(); ?>
