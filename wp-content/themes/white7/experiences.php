<?php
/**
 * Template Name: Experiences Page
  * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

//get_header(); ?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">  
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,700' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" type="image/x-icon">
<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" type="image/x-icon">
<link href="<?php echo get_template_directory_uri(); ?>/css/template.css" type="text/css" rel="stylesheet" />


<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/srollingjs/jquery-1.4.2.js"></script>

<script>
$(document).ready(function() {

	$('#menu-toggle-harish').click(function () {
      $('#menuharish').toggleClass('open');
/*
      e.preventDefault();
*/
    });
    
});
</script>



	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="header">

<div class="mx_width">

<?php
if ( is_page( '10' ) ) {?>

 <div class="headerleft"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logo2.jpg" /></a></div>

<?php } else {?>
 <div class="headerleft"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" /></a></div>

<?php }
?>

 
  <div class="headerright">
  <a id="menu-toggle-harish" class="anchor-link" href="#">Navigation</a>
      <ul class="socialicon">

        <li class="fb"><a href="https://www.facebook.com/pages/White-7/332879043522236" target="_blank">Facebook</a></li>

        <li class="tw"><a href="https://twitter.com/white7_" target="_blank">Twitter</a></li>

        <li class="gp"><a href="https://plus.google.com/104232058787244802752" target="_blank">Google Plus</a></li>

      </ul>
  <div id="menuharish">
  <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
  </div>
  </div>
<div class="cl"></div>
</div>



</div>













<div id="main-content" class="bg">

<?php
	if ( is_front_page() && twentyfourteen_has_featured_posts() ) {
		// Include the featured content template.
		get_template_part( 'featured-content' );
	}
?>

	<div id="primary" class="mx_width">
		<div class="weddingpage experience_page" role="main">
        

 <div class="sidebar-right">
<div class="weddingpageexprince_new">
 
<?php
				// Start the Loop.
				while ( have_posts() ) : the_post();

					// Include the page content template.
					get_template_part( 'content', 'page' );

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) {
						//comments_template();
					}
				endwhile;
			?>
			 <div class="cl"></div>

 <div id="verticalTab">
        <ul class="resp-tabs-list paymentOption">
            <?php
   global $post;;
query_posts( array('post_type'=>'expabvontent','order'=>'ASC'));
if ( have_posts() ) :
	while ( have_posts() ) : the_post();
		?>  <li class="TabbedPanelsTab" tabindex="0"><?php the_title() ?> </li>


<?php
	endwhile;
endif;
wp_reset_query();
?>

        </ul>
        
        <div class="resp-tabs-container">
        <?php
   global $post;;
query_posts( array('post_type'=>'expabvontent','order'=>'ASC'));
if ( have_posts() ) :
	while ( have_posts() ) : the_post();
		?> <div class="TabbedPanelsContent"> 
		
        
        <h1><?php the_title() ?> </h1>
		<?php the_content() ?>  
       <div class="tabimg">
        <?php
$images = miu_get_images($post->ID);
foreach ($images as $image):
    ?>
<div class="wedimg">  <a href="<?php echo $image; ?>" class="fbx-instance fbx-link"> <img src="<?php echo $image; ?>" class="alignnone size-full wp-image-110" alt="" /> </a><br />
<span> click image to enlarge</span></div>
<?php endforeach; ?> 
       </div>
       
       <div class="cl"></div> 
        </div>
        
        

<?php
	endwhile;
endif;
wp_reset_query();
?>
        </div>
      </div>
</div>
</div>

 <?php 
 global $post;
 $meta_values = get_post_meta($post->ID,'wpcf-brochure-uploads',$single);
 
 //echo "<pre>";
 //print_r($meta_values);
  ?> 

 
  <div class="weddingpageright expriingpage">
  
<div class="expricesimg" style="text-align: justify;">
<div class="row">
	<img class="alignnone size-full wp-image-159" src="<?=site_url()?>/wp-content/uploads/2014/09/experience-3.jpg" alt="experience-3" width="127" height="126" />
	<img class="alignnone size-full wp-image-160" src="<?=site_url()?>/wp-content/uploads/2014/09/experience-4.jpg" alt="experience-4" width="127" height="126" />
</div>
<div class="row">
	<img class="alignnone size-full wp-image-157" src="<?=site_url()?>/wp-content/uploads/2014/09/experience-1.jpg" alt="experience-1" width="127" height="126" />
	<img class="alignnone size-full wp-image-162" src="<?=site_url()?>/wp-content/uploads/2014/09/experience-6.jpg" alt="experience-6" width="127" height="126" />
</div>
<div class="row">
	<img class="alignnone size-full wp-image-161" src="<?=site_url()?>/wp-content/uploads/2014/09/experience-5.jpg" alt="experience-5" width="127" height="126" /> 
	<img class="alignnone size-full wp-image-158" src="<?=site_url()?>/wp-content/uploads/2014/09/experience-2.jpg" alt="experience-2" width="127" height="126" />
</div>
</div>
 <!-- 
<div>
                <div class="" style="text-align:  center;">
                    <h2 style="text-align: center">Download</h2>
                    <a href="<?=$meta_values?>" target="_blank" style="margin-top: 0; width: 100%;" class="engBtn">Corporate Brochure - PDF</a>
                </div></div>
 -->
 <div class="cl"></div>
 		<img class="" style="width:  265px;margin-top:  110px;" src="<?=get_stylesheet_directory_uri()?>/images/white-7-experiences_harrods-Ritz.jpg" alt="experience-2" />

 </div>
 
 <div class="cl"></div>
 
  </div>
    
    <!-- #content -->
   
 <!--<div class="enquire">
<div class="button-enquire">
  <a  href="<?php echo esc_url( home_url( '/experiences-enquiry-form' ) ); ?>"><span>Enquire</span></a>
  </div> </div>-->
        
	</div><!-- #primary -->
	<?php get_sidebar( 'content' ); ?>
</div><!-- #main-content -->



<?php
//get_sidebar();
get_footer(); ?>



<script src="<?php echo get_template_directory_uri(); ?>/responsive-tab/jquery.1.10.2.js" type="text/javascript"></script>

<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/responsive-tab/easy-responsive-tabs.css" />
<script src="<?php echo get_template_directory_uri(); ?>/responsive-tab/easyResponsiveTabs.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#horizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion           
            width: 'auto', //auto or any width like 600px
            fit: true,   // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            activate: function(event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#tabInfo');
                var $name = $('span', $info);

                $name.text($tab.text());

                $info.show();
            }
        });

        $('#verticalTab').easyResponsiveTabs({
            type: 'vertical',
            width: 'auto',
            fit: true
        });
    });
</script>