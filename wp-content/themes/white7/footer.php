<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>

		
        
        

<div class="mx_width">
		<div class="footerIn">
<div class="corporatepartners">
<div class="contenttitle"> Our Corporate Partners</div>

<ul><li> <a href="http://www.rybrookshrewsburybmw.co.uk/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/patners-1.jpg" alt="" height="93" /> </a> </li>
<li> <a href="http://www.weddinginsurancegroup.co.uk/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/patners-2.jpg" alt="" height="93" /> </a> </li>

</ul>
<!--<ul><?php
$images = miu_get_images($post_id=22);
foreach ($images as $image):
    ?>
  <li>   <img src="<?php echo $image; ?>" alt="" height="93" /> </li>
<?php endforeach; ?>
 </ul>-->
</div>


<div class="associates">
<div class="contenttitle"> Our Associates</div>
<ul>
<li><a href="http://www.matm.co.uk/" target="_blank" rel="nofollow"> <img src="<?php echo get_template_directory_uri(); ?>/images/matm.jpg" alt="" height="93" /></a> </li>
<!-- 
<li> <a href="http://www.tranterlowe.com/" target="_blank" rel="nofollow"><img src="<?php echo get_template_directory_uri(); ?>/images/ourassociate.2png.png" alt="" height="93" /></a> </li>
 -->
<li> <a href="http://www.britishchauffeursguild.co.uk/" target="_blank" rel="nofollow"><img src="<?php echo get_template_directory_uri(); ?>/images/ourassociate.3png.png" alt="" height="93" /> </a> </li>


</ul>
<!--<ul>
<?php
$images = miu_get_images($post_id=32);
foreach ($images as $image):
    ?>
  <li>   <img src="<?php echo $image; ?>" alt="" height="93" /> </li>
<?php endforeach; ?>
</ul>-->

</div>



<div class="cl"></div>
</div>

<div class="copy">© <?=date('Y')?> White 7 UK Ltd </div>
<div class="footerMenu" style=" color:#fff;"><a href="<?php echo esc_url( home_url( '/terms-conditions' ) ); ?>">Terms & Conditions</a> |  <a href="http://www.white7.co.uk/sitemap/">Sitemap</a> | <a href="<?=site_url()?>/privacy-policy/">Privacy Policy</a></div>

</div>

</div>
	

	<?php wp_footer(); ?>



</body>

</html>
