<?php


	/**
	 * Template Name: Homepage
	 *
	 * @package WordPress
	 * @subpackage Twenty_Fourteen
	 * @since Twenty Fourteen 1.0
	 */


	get_header(); ?>

<div class="banner">
    <div class="mx_width">
        <div id="slideshow" class="pics">
			<?php


				$images = miu_get_images( $post_id = 44 );


				foreach ( $images as $image ):


					?>
                    <div><img src="<?php echo str_replace("http://", "https://", $image); ?>" width="1152" height="532" alt=""/></div>
				<?php endforeach; ?>
        </div>

        <!--<div class="slogn">Your Chauffeur Awaits</div>-->

        <div class="wordsmenu">
        </div>
    </div>
</div>
<div class="welcomeText">
    <div class="inner-container">
        <script>

            $(document).ready(function () {

                $(".show").click(function () {

                    $("#producall").addClass("activelist");

                    $(".show").addClass("plus");

                    $(".hide").addClass("minus");

                });

                $(".hide").click(function () {

                    $("#producall").removeClass("activelist");

                    $(".show").removeClass("plus");

                    $(".hide").removeClass("minus");


                });

            });

        </script>
        <div class="mx_width">
		    <?php

			    $page_id = 101;

			    $page_data = get_page( $page_id );

			    $content = apply_filters('the_content', $page_data->post_content);

			    $title = $page_data->post_title;

			    echo $content;

		    ?>
        </div>
        <a href="<?=site_url()?>/chauffeured-vehicles/">
            <div class="one_half" id="first_box">
                <div class="home_box_container">
                    <div class="header_box">
                        <div class="left_box">
                            <h3>Chauffeured Vehicles</h3>
                        </div>
                            <div class="right_box">
                                <p class="found_out_more">Find Out More</p>
                            </div>

                    </div>
                    <div class="main_image_holder">
                        <img src="<?=get_template_directory_uri()?>/images/White7-Chauffeured-Vehicles.jpg" alt="White 7 Chauffeured Vehicles">
                    </div>
                </div>
            </div>
        </a>

        <a href="<?=site_url()?>/franchising/">
            <div class="one_half" id="second_box">
                <div class="home_box_container">
                    <div class="header_box">
                        <div class="left_box">
                            <h3>Become a Franchisee</h3>
                        </div>

                            <div class="right_box">
                                <p class="found_out_more">Find Out More</p>
                            </div>
                    </div>
                    <div class="main_image_holder">
                        <img src="<?=get_template_directory_uri()?>/images/White-7-Franchisee_hat.jpg" alt="Become a White 7 Franchisee">
                    </div>
                </div>
            </div>
        </a>
        <img src="<?=site_url()?>/wp-content/uploads/2014/09/air_ambulance.jpg" style="width: 100%;" alt="White 7 with Midlands Air Ambulance">
    </div>
</div>
<div class="content">
    <div class="mx_width">
        <div class="corporatepartners">
            <div class="contenttitle"> Our Corporate Partners</div>
            <ul>
                <li><a href="http://www.rybrookshrewsburybmw.co.uk/" target="_blank"><img
                                src="<?php echo get_template_directory_uri(); ?>/images/patners-1.jpg" alt=""
                                height="93"/> </a></li>
                <li><a href="http://www.weddinginsurancegroup.co.uk/" target="_blank"><img
                                src="<?php echo get_template_directory_uri(); ?>/images/patners-2.jpg" alt=""
                                height="93"/> </a></li>
            </ul>

            <!--<ul><?php


				$images = miu_get_images( $post_id = 22 );


				foreach ( $images as $image ):


					?>



  <li>   <img src="<?php echo $image; ?>" alt="" height="93" /> </li>



<?php endforeach; ?>



 </ul>-->

        </div>
        <div class="associates">
            <div class="contenttitle"> Our Associates</div>
            <ul>
                <li><a href="http://www.matm.co.uk/" target="_blank" rel="nofollow"> <img
                                src="<?php echo get_template_directory_uri(); ?>/images/matm.jpg" alt=""
                                height="93"/></a></li>
                <li><a href="http://www.britishchauffeursguild.co.uk/" target="_blank" rel="nofollow"><img
                                src="<?php echo get_template_directory_uri(); ?>/images/ourassociate.3png.png" alt=""
                                height="93"/> </a></li>
            </ul>

            <!--<ul>



<?php


				$images = miu_get_images( $post_id = 32 );


				foreach ( $images as $image ):


					?>



  <li>   <img src="<?php echo $image; ?>" alt="" height="93" /> </li>



<?php endforeach; ?>



</ul>



-->

        </div>
        <div class="cl"></div>
    </div>
</div>
<div class="footer">
    <div class="mx_width">
        <div class="aboutwhite">
            <div class="fooertitle"><?php echo get_the_title( 41 ); ?></div>
            <div class="home-aboutcontent">
				<?php


					$page_id = 41;


					$page_data = get_page( $page_id );


					$content = apply_filters( 'the_content', $page_data->post_content );


					$title = $page_data->post_title;


					echo do_shortcode( $content );


				?>
            </div>
            <div class="readmore"><a href="<?php echo get_page_link( 41 ); ?>">Read more..</a></div>

        </div>
        <div class="awards">
            <div class="fooertitle">Awards</div>
            <ul>
                <a href="<?=site_url()?>/white-7-brings-gold-award-back-to-the-county/" title="White 7 Brings Gold Award Back To The County" rel="bookmark">
                    <img src="<?=site_url()?>/wp-content/uploads/2014/10/QSI-logo-2018-Awards_300.png" class="qsi-logo" />
                    <li>White 7 Brings Gold Award Back To The County</li>
                </a>
            </ul>
            <div class="cl"></div>
        </div>
        <div class="news">
            <div class="fooertitle">News</div>
            <ul>
				<?php


					query_posts( 'cat=1&showposts=4' );


					while ( have_posts() ) : the_post();


//the_title();


						{
							?>
                            <li><a href="<?php the_permalink(); ?>"
                                   title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'twentytwelve' ), the_title_attribute( 'echo=0' ) ) ); ?>"
                                   rel="bookmark">
									<?php the_title(); ?>
                                </a></li>
						<?php } ?>
					<?php


					endwhile;


				?>
                <li><a href="<?=site_url()?>/news/" rel="bookmark">
                        Blog
                    </a></li>
            </ul>
            <div class="cl"></div>
        </div>
        <div class="cl"></div>

        <!--<div class="footerMenu" style=" color:#fff;">



	<a href="http://www.white7.co.uk/airport-transfer/">Airport Transfer</a>  |



	<a href="http://www.white7.co.uk/executive-travel/">Executive Travel</a>  |



	<a href="http://www.white7.co.uk/wedding-car/">Weddings</a>  |



	<a href="http://www.white7.co.uk/about-us/">About Us</a>  |



	<a href="http://www.white7.co.uk/contact-us/">Contact Us</a>







	</div>-->

        <p style="padding-top:10px;"></p>
        <div class="cl"></div>
        <div class="copy">© <?=date('Y')?> White 7 UK Ltd</div>
        <div class="footerMenu" style=" color:#fff;"><a
                    href="<?php echo esc_url( home_url( '/terms-conditions' ) ); ?>">Terms & Conditions</a> | <a
                    href="http://www.white7.co.uk/sitemap/">Sitemap</a></div>
    </div>
</div>
<?php wp_footer(); ?>
<?php


	//get_sidebar();


	//get_footer();


?>
