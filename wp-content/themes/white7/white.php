<?php
/**
 * Template Name: White Letter Days
  * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<script src="<?php echo get_template_directory_uri(); ?>/SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
<link href="<?php echo get_template_directory_uri(); ?>/SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />

<div id="main-content" class="bg">

<?php
	if ( is_front_page() && twentyfourteen_has_featured_posts() ) {
		// Include the featured content template.
		get_template_part( 'featured-content' );
	}
?>

	<div id="primary" class="mx_width">
		<div class="weddingpage" role="main">
        

 <div class="weddingpageleft">
<div class="weddingpageexprince">
 
<?php
				// Start the Loop.
				while ( have_posts() ) : the_post();

					// Include the page content template.
					get_template_part( 'content', 'page' );

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) {
						//comments_template();
					}
				endwhile;
			?>
</div>
</div>

 <?php 
 global $post;
 $meta_values = get_post_meta($post->ID,'wpcf-brochure-uploads',$single);
 
 //echo "<pre>";
 //print_r($meta_values);
  ?> 

 
  <div class="weddingpageright">
 <div class="downloadbox">
 <div class="downloadboxleft"><img src="<?php echo get_template_directory_uri(); ?>/images/corportatebr.jpg" /></div>
 <div class="downloadboxright"> 
 <div class="downoption"><a href="<?php echo $meta_values;?>" target="_blank">Download
<span>  White 7 Corporate Brochure </span></a></div></div>
 <div class="cl"></div>
 </div>
 
 <div class="cl"></div>
 
  </div>
  
  
  <div class="cl"></div>

 
    
    <div class="tabmenu">  
    <div id="TabbedPanels1" class="TabbedPanels">
    <ul class="TabbedPanelsTabGroup">
       
      
   <?php
   global $post;;
query_posts( array('post_type'=>'whitetabcontent','order'=>'ASC'));
if ( have_posts() ) :
	while ( have_posts() ) : the_post();
		?>  <li class="TabbedPanelsTab" tabindex="0"><?php the_title() ?> </li>


<?php
	endwhile;
endif;
wp_reset_query();
?>

   </ul>
   
   <div class="TabbedPanelsContentGroup">
        
      <?php
   global $post;;
query_posts( array('post_type'=>'whitetabcontent','order'=>'ASC'));
if ( have_posts() ) :
	while ( have_posts() ) : the_post();
		?> <div class="TabbedPanelsContent"> 
		
        
        
		<?php the_content() ?>  
       <div class="tabimg">
        <?php
$images = miu_get_images($post->ID);
foreach ($images as $image):
    ?>


  <a href="<?php echo $image; ?>" class="fbx-instance fbx-link"> <img src="<?php echo $image; ?>" class="alignnone size-full wp-image-110" alt="" /> </a>
  
<?php
$thumb_id = get_post_thumbnail_id($post->id);
$alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
echo $alt;
?>
  
<?php endforeach; ?> 


       </div> 
        </div>
        
        

<?php
	endwhile;
endif;
wp_reset_query();
?>

</div>




</div>

  
  
  
<div class="cl"></div>

 
 






        
	

		</div><!-- #content -->
        
        

 <div class="enquire">
<div class="button-enquire">
  <a  href="<?php echo esc_url( home_url( '/corporate-enquiry-form' ) ); ?>"><span>Enquire</span></a>
  </div> </div>
        
	</div><!-- #primary -->
	<?php get_sidebar( 'content' ); ?>
</div><!-- #main-content -->

<script type="text/javascript">
<!--
var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
//-->
</script>

<?php
//get_sidebar();
get_footer();
