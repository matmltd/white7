<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

	<div class="bg">
    
    <div id="primary" class="mx_width">
		<div id="content" role="main">
		<div class="innerLeft fl">
        <div class="weddingpage">
        	<?php
				// Start the Loop.
				while ( have_posts() ) : the_post();

					/*
					 * Include the post format-specific template for the content. If you want to
					 * use this in a child theme, then include a file called called content-___.php
					 * (where ___ is the post format) and that will be used instead.
					 */
					get_template_part( 'content', get_post_format() );

					// Previous/next post navigation.
					//twentyfourteen_post_nav();

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) {
						//comments_template();
					}
				endwhile;
			?></div></div>
          
          
<div class="rightpartin singlepage fr">
<div class="shedowtop"> asa</div>
<div class="rightTitle">RECENT NEWS</div>
  <ul class="singlepagelist">
  
  <?php
query_posts('cat=1&showposts=7'); 
while (have_posts()) : the_post();


//the_title();
{?>


<li> 
<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'twentytwelve' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
	</li>
            
    <?php }?>

  <?php       
endwhile;
?>
</ul>

 <div class="rightTitle">Awards</div>
  <ul class="singlepagelist">
 <?php
query_posts('cat=4&showposts=4'); 
while (have_posts()) : the_post();


//the_title();
{?>


<li> 
<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'twentytwelve' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
	</li>
            
    <?php }?>

  <?php       
endwhile;
?>
  <div class="cl"></div>
  </ul>
</div>
          
            <?php //get_sidebar(); ?>

<div class="cl"></div>
		</div><!-- #content -->
	</div><!-- #primary -->

<?php
//get_sidebar( 'content' );
//get_sidebar();
get_footer();
