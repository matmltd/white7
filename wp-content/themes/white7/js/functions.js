(function($){var body=$('body'),_window=$(window);(function(){var nav=$('#primary-navigation'),button,menu;if(!nav){return;}button=nav.find('.menu-toggle');if(!button){return;}menu=nav.find('.nav-menu');if(!menu||!menu.children().length){button.hide();return;}$('.menu-toggle').on('click.twentyfourteen',function(){nav.toggleClass('toggled-on');});})();_window.on('hashchange.twentyfourteen',function(){var hash=location.hash.substring(1),element;if(!hash){return;}element=document.getElementById(hash);if(element){if(!/^(?:a|select|input|button|textarea)$/i.test(element.tagName)){element.tabIndex=-1;}element.focus();window.scrollBy(0,-80);}});$(function(){$('.search-toggle').on('click.twentyfourteen',function(event){var that=$(this),wrapper=$('.search-box-wrapper');that.toggleClass('active');wrapper.toggleClass('hide');if(that.is('.active')||$('.search-toggle .screen-reader-text')[0]===event.target){wrapper.find('.search-field').focus();}});if(_window.width()>781){var mastheadHeight=$('#masthead').height(),toolbarOffset,mastheadOffset;if(mastheadHeight>48){body.removeClass('masthead-fixed');}if(body.is('.header-image')){toolbarOffset=body.is('.admin-bar')?$('#wpadminbar').height():0;mastheadOffset=$('#masthead').offset().top-toolbarOffset;_window.on('scroll.twentyfourteen',function(){if(_window.scrollTop()>mastheadOffset&&mastheadHeight<49){body.addClass('masthead-fixed');}else{body.removeClass('masthead-fixed');}});}}$('.primary-navigation, .secondary-navigation').find('a').on('focus.twentyfourteen blur.twentyfourteen',function(){$(this).parents().toggleClass('focus');});});_window.load(function(){if($.isFunction($.fn.masonry)){$('#footer-sidebar').masonry({itemSelector:'.widget',columnWidth:function(containerWidth){return containerWidth/4;},gutterWidth:0,isResizable:true,isRTL:$('body').is('.rtl')});}if(body.is('.slider')){$('.featured-content').featuredslider({selector:'.featured-content-inner > article',controlsContainer:'.featured-content'});}});})(jQuery);
var hover = 0;
/* MENU */


function FullyOpen(element) {
    console.log(element);
    element.find('ul').eq(0).css('overflow', 'visible');
}

function resizeboxes() {
    var biggest = 0,
        links = $('.areas a');
    links.each(function () {

        $(this).css('height', '');

        if($(this).height() > biggest){
            biggest = $(this).height();
        }
    });

    links.each(function () {
        $(this).css('height', biggest);
    });
}

jQuery(document).ready(function () {
    jQuery("#menu-header-menu > li > ul").each(function () {
        jQuery(this).data('height',jQuery(this).outerHeight());
        jQuery(this).css('max-height', 0);
        jQuery(this).find('ul').eq(0).css("opacity", "0");
        jQuery(this).css('border', 'none');
    });
    jQuery("#menu-header-menu > li > ul > li > ul").each(function () {
        jQuery(this).data('height', jQuery(this).outerHeight());
        jQuery(this).css('max-height', 0);
        jQuery(this).css("opacity", "0");
        jQuery(this).css('border', 'none');
    });

    var subtimeout;
    var subsubtimeout;
    jQuery("#menu-header-menu > li").hover(function () {
        jQuery(this).find('ul').eq(0).css('border', '');
        jQuery(this).find('ul').eq(0).addClass("hovered");
        theheight = jQuery(this).find('ul').eq(0).data('height');
        jQuery(this).find('ul').eq(0).css("max-height", theheight);
        jQuery(this).find('ul').eq(0).css("overflow", "hidden");
        jQuery(this).find('ul').eq(0).css("opacity", "1");
        element = jQuery(this);
        subtimeout = setTimeout(function () {
            FullyOpen(element)
        }, 500);
    }, function () {
        jQuery(this).find('ul').eq(0).removeClass("hovered");
        jQuery(this).find('ul').eq(0).css("max-height", 0);
        jQuery(this).find('ul').eq(0).css("overflow", "hidden");
        jQuery(this).find('ul').eq(0).css("opacity", "0");
        clearTimeout(subtimeout);
    });

    jQuery("#menu-header-menu > li > ul > li").hover(function () {
        jQuery(this).find('ul').eq(0).css('border', '');
        jQuery(this).find('ul').eq(0).addClass("hovered");
        theheight = jQuery(this).find('ul').eq(0).data('height');
        jQuery(this).find('ul').eq(0).css("max-height", theheight);
        jQuery(this).find('ul').eq(0).css("overflow", "hidden");
        jQuery(this).find('ul').eq(0).css("opacity", "1");
       /* subtimeout = setTimeout(function () {
            FullyOpen(jQuery(this).find('ul').eq(0))
        }, 500);*/
    }, function () {
        jQuery(this).find('ul').eq(0).removeClass("hovered");
        jQuery(this).find('ul').eq(0).css("max-height", 0);
        jQuery(this).find('ul').eq(0).css("overflow", "hidden");
        jQuery(this).find('ul').eq(0).css("opacity", "0");
/*
        clearTimeout(subsubtimeout);
*/

    });
    jQuery("#menu-header-menu li.menu-item-has-children").each(function () {
        $(this).data('clicked', 0);
    });


    jQuery("#menu-header-menu li.menu-item-has-children").click(function () {
        if(jQuery(window).width() < 1200){
/*
            console.log($(this).data('clicked'));
*/

            if($(this).data('clicked') == 0){
                $(this).data('clicked', 1);
                return false;
            } else if( $(this).data('clicked') == 1){
                $(this).data('clicked', 2);
                jQuery(this).find('ul').eq(0).removeClass("hovered");
                jQuery(this).find('ul').eq(0).css("max-height", 0);
                jQuery(this).find('ul').eq(0).css("overflow", "hidden");
                jQuery(this).find('ul').eq(0).css("opacity", "0");
                return false;
            } else {
                $(this).data('clicked', 0);
            }
        }
    });

    jQuery("#menu-header-menu").addClass("initialised");

});