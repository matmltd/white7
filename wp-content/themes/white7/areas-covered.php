<?php
	/**
	 * The template for displaying all pages
	 *
	 * This is the template that displays all pages by default.
	 * Please note that this is the WordPress construct of pages and that
	 * other 'pages' on your WordPress site will use a different template.
	 *
	 * @package WordPress
	 * @subpackage Twenty_Fourteen
	 * @since Twenty Fourteen 1.0
	 */

	/* template name: Areas Covered */

	get_header(); ?>

	<script>
        $( window ).resize(function() {
            resizeboxes();
        });
        $( window ).load(function() {
            resizeboxes();
        });
	</script>

<div id="main-content" class="bg">

	<?php
		if ( is_front_page() && twentyfourteen_has_featured_posts() ) {
			// Include the featured content template.
			get_template_part( 'featured-content' );
		}
	?>
	<div id="primary" class="mx_width">


		<div class="weddingpage" role="main">

			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post();

					// Include the page content template.
					get_template_part( 'content', 'page' );

					?>

					<div class="areas">
						<?php
							$args = array(
								'post_type'   => 'franchise-area',
								'numberposts' => -1,
								'post_status' => 'publish'
							);
							$children = get_children( $args );

							foreach ($children as $page_id => $child) {
								$custom = get_post_custom($page_id);
								?>
						<a href="<?=$custom['site_link'][0]?>" class="area_link">
							<div class="area" id="<?=$page_id?>">
								<div class="left_side" style="background-image:  url('<?=get_the_post_thumbnail_url($page_id)?>');">
									<!--<img src="<?=get_the_post_thumbnail_url($page_id)?>" class="area_img"/>-->
								</div>
								<div class="right_side">
									<h2><?=$child->post_title?></h2>
									<p><?=$child->post_excerpt?></p>
								</div>
							</div>
						</a>
								<?php
							}
						?>
					</div>

					<?php
				endwhile;
			?>

			</div><!-- #content -->


		<div class="cl"></div>
	</div><!-- #primary -->

	<!-- #main-content -->

	<?php
		//get_sidebar();
		get_footer(); ?>