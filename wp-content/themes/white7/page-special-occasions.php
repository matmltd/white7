<?php
/**
 * Template Name: Corporate Page
  * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

//get_header(); ?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">  
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,700' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" type="image/x-icon">
<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" type="image/x-icon">
<link href="<?php echo get_template_directory_uri(); ?>/css/template.css" type="text/css" rel="stylesheet" />


<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/srollingjs/jquery-1.4.2.js"></script>

<script>
$(document).ready(function() {

	$('#menu-toggle-harish').click(function () {
      $('#menuharish').toggleClass('open');
/*
      e.preventDefault();
*/
    });
    
});
</script>



	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="header">

<div class="mx_width">

<?php
if ( is_page( '10' ) ) {?>

 <div class="headerleft"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logo1.png" /></a></div>

<?php } else {?>
 <div class="headerleft"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" /></a></div>

<?php }
?>

 
  <div class="headerright">
  <a id="menu-toggle-harish" class="anchor-link" href="#">Navigation</a>
  
      <ul class="socialicon">

        <li class="fb"><a href="https://www.facebook.com/pages/White-7/332879043522236" target="_blank">Facebook</a></li>

        <li class="tw"><a href="https://twitter.com/white7_" target="_blank">Twitter</a></li>

        <li class="gp"><a href="https://plus.google.com/104232058787244802752" target="_blank">Google Plus</a></li>

      </ul>
  <div id="menuharish">
  <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
  </div>
  </div>
<div class="cl"></div>
</div>



</div>










<div id="main-content" class="bg">

<?php
	if ( is_front_page() && twentyfourteen_has_featured_posts() ) {
		// Include the featured content template.
		get_template_part( 'featured-content' );
	}
?>

	<div id="primary" class="mx_width">
		<div class="specialoccasions" role="main">
        

     <?php
   global $post;;
query_posts( array('post_type'=>'specialoccasions', 'posts_per_page' => '1','order'=>'ASC'));
if ( have_posts() ) :
	while ( have_posts() ) : the_post();
		?> 
           <?php // the_ID(); ?> 
 <div class="offer<?php the_ID() ?>">
 
  <?php //the_title() ?>   <?php the_content() ?>
</div>
<?php
	endwhile;
endif;
wp_reset_query();
?>

 

 

  
  
  <div class="cl"></div>

 
 
 
    
    <!-- #content -->
        
        

 <!--<div class="enquire">
<div class="button-enquire">
  <a  href="<?php echo esc_url( home_url( '/corporate-enquiry-form' ) ); ?>"><span>Enquire</span></a>
  </div> </div>-->
        
	</div><!-- #primary -->
	<?php get_sidebar( 'content' ); ?>
</div><!-- #main-content -->



<?php
//get_sidebar();
get_footer(); ?>









<script src="<?php echo get_template_directory_uri(); ?>/responsive-tab/jquery.1.10.2.js" type="text/javascript"></script>

<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/responsive-tab/easy-responsive-tabs.css" />
<script src="<?php echo get_template_directory_uri(); ?>/responsive-tab/easyResponsiveTabs.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#horizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion           
            width: 'auto', //auto or any width like 600px
            fit: true,   // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            activate: function(event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#tabInfo');
                var $name = $('span', $info);

                $name.text($tab.text());

                $info.show();
            }
        });

        $('#verticalTab').easyResponsiveTabs({
            type: 'vertical',
            width: 'auto',
            fit: true
        });
    });
</script>