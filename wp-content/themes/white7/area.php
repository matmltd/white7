<?php
	/**
	 * The template for displaying all pages
	 *
	 * This is the template that displays all pages by default.
	 * Please note that this is the WordPress construct of pages and that
	 * other 'pages' on your WordPress site will use a different template.
	 *
	 * @package WordPress
	 * @subpackage Twenty_Fourteen
	 * @since Twenty Fourteen 1.0
	 */

	/* template name: Area Page */

	get_header(); ?>

	<div id="main-content" class="bg">

<?php
	if ( is_front_page() && twentyfourteen_has_featured_posts() ) {
		// Include the featured content template.
		get_template_part( 'featured-content' );
	}
?>
	<div id="primary" class="mx_width">


		<div class="weddingpage" role="main">

			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post();

					$custom = get_post_custom();
					?>
					<script>
						window.location = "<?=$custom['site_link'][0]?>";
					</script>
					<?php

				endwhile;
			?>

			<div class="areas">
				<div class="area" id="">

				</div>
			</div>

		</div><!-- #content -->


		<div class="cl"></div>
	</div><!-- #primary -->

	<!-- #main-content -->

<?php
	//get_sidebar();
	get_footer(); ?>