<?php
/**
 * The template for displaying Category pages
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>


	<div class="bg">
    
    <div id="primary" class="mx_width">
		<div id="content" role="main">
        
        
        <div class="innerLeft fl">

<div class="weddingpage">
			<?php if ( have_posts() ) : ?>

				<h1 class="archive-title"><?php printf( __( 'Category Archives: %s', 'twentyfourteen' ), single_cat_title( '', false ) ); ?></h1>
			
<br />
<br />

				<?php
					// Show an optional term description.
					$term_description = term_description();
					if ( ! empty( $term_description ) ) :
						printf( '<div class="taxonomy-description">%s</div>', $term_description );
					endif;
				?>
		

			<?php
					// Start the Loop.
					while ( have_posts() ) : the_post();

					/*
					 * Include the post format-specific template for the content. If you want to
					 * use this in a child theme, then include a file called called content-___.php
					 * (where ___ is the post format) and that will be used instead.
					 */
					get_template_part( 'content', get_post_format() );

					endwhile;
					// Previous/next page navigation.
					twentyfourteen_paging_nav();

				else :
					// If no content, include the "No posts found" template.
					get_template_part( 'content', 'none' );

				endif;
			?>
</div>	</div>	<!-- #content -->

   
    
    <div class="rightpartin singlepage fr">
<div class="shedowtop"> asa</div>
<div class="rightTitle">RECENT NEWS</div>
  <ul class="singlepagelist">
  
  <?php
query_posts('cat=1&showposts=7'); 
while (have_posts()) : the_post();


//the_title();
{?>


<li> 
<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'twentytwelve' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
	</li>
            
    <?php }?>

  <?php       
endwhile;
?>
</ul>

 <div class="rightTitle">Awards</div>
  <ul class="singlepagelist">
 <?php
query_posts('cat=4&showposts=4'); 
while (have_posts()) : the_post();


//the_title();
{?>


<li> 
<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'twentytwelve' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
	</li>
            
    <?php }?>

  <?php       
endwhile;
?>
  <div class="cl"></div>
  </ul>
</div>
 </div>

<div class="cl"></div>
</div>

<?php
//get_sidebar( 'content' );
//get_sidebar();
get_footer();


