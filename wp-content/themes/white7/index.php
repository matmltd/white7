<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one
 * of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query,
 * e.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>



<?php
	if ( is_front_page() && twentyfourteen_has_featured_posts() ) {
		// Include the featured content template.
		get_template_part( 'featured-content' );
	}
?>


	<div class="bg">
    
    <div id="primary" class="mx_width">
		<div id="content" role="main">
    
        <div class="innerLeft fl">

<div class="weddingpage">
		<?php
			if ( have_posts() ) :
				// Start the Loop.
				while ( have_posts() ) : the_post();

					/*
					 * Include the post format-specific template for the content. If you want to
					 * use this in a child theme, then include a file called called content-___.php
					 * (where ___ is the post format) and that will be used instead.
					 */
					get_template_part( 'content', get_post_format() );

				endwhile;
				// Previous/next post navigation.
				twentyfourteen_paging_nav();

			else :
				// If no content, include the "No posts found" template.
				get_template_part( 'content', 'none' );

			endif;
		?>

		</div><!-- #content -->
	</div><!-- #primary -->
	<?php //get_sidebar( 'content' ); ?>



   <div class="rightpartin singlepage fr">
<div class="shedowtop"> asa</div>
<div class="rightTitle">RECENT NEWS</div>
  <ul class="singlepagelist">
  
  <?php
query_posts('cat=1&showposts=7'); 
while (have_posts()) : the_post();


//the_title();
{?>


<li> 
<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'twentytwelve' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
	</li>
            
    <?php }?>

  <?php       
endwhile;
?>
</ul>

 <div class="rightTitle">Awards</div>
  <ul class="singlepagelist">
 <?php
query_posts('cat=4&showposts=4'); 
while (have_posts()) : the_post();


//the_title();
{?>


<li> 
<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'twentytwelve' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
	</li>
            
    <?php }?>

  <?php       
endwhile;
?>
  <div class="cl"></div>
  </ul>
</div>

 </div>

<div class="cl"></div>
</div>

<?php
//get_sidebar();
get_footer();
