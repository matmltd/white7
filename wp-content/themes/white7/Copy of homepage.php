<?php
/**
 * Template Name: Homepage
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div class="banner">
<div class="mx_width">
  
<div id="slideshow" class="pics">
          
<?php
$images = miu_get_images($post_id=44);
foreach ($images as $image):
    ?>
  <div>   <img src="<?php echo $image; ?>" width="1152" height="532" alt="" /> </div>
<?php endforeach; ?>    </div> 

<div class="slogn">Your Chauffeur Awaits</div>
<div class="wordsmenu">
<marquee  scrollamount="2" scrolldelay="5" direction="left" onmouseover="this.stop()" onmouseout="this.start()">
<ul> 
<?php
query_posts('cat=1&showposts=8');
while (have_posts()) : the_post();
//the_title();
{?>

<li> 
<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'twentytwelve' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
	</li>
            
    <?php }?>

  <?php       
endwhile;
?>



<?php //wp_nav_menu( array( 'theme_location' => 'homepage-scrolling' ) ); ?>

<!--<li>-   <a href="#">New Arrivals</a></li>
<li>-   <a href="#">Kined Words</a></li>
<li>-   <a href="#">Mayor's Rcognition</a></li>
<li>-   <a href="#">Many Thanks</a></li>
<li>-   <a href="#">Shooters Hill Hall</a></li>
<li>-   <a href="#">Kind Words</a></li>
<li>-   <a href="#">White 7 & Helitrip</a></li>
<li>-   <a href="#">Shooters Hill Hall</a></li>-->
<div class="cl"></div>
</ul>
</marquee>
</div>

</div>
</div>

<div class="content">
<div class="mx_width">

<div class="corporatepartners">
<div class="contenttitle"> Our Corporate Partners</div>

<ul><li> <a href="http://www.rybrookshrewsburybmw.co.uk/" rel="nofollow" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/patners-1.jpg" alt="" height="93" /> </a> </li>
<li> <a href="http://www.weddinginsurancegroup.co.uk/" rel="nofollow" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/patners-2.jpg" alt="" height="93" /> </a> </li>

</ul>
<!--<ul><?php
$images = miu_get_images($post_id=22);
foreach ($images as $image):
    ?>
  <li>   <img src="<?php echo $image; ?>" alt="" height="93" /> </li>
<?php endforeach; ?>
 </ul>-->
</div>


<div class="associates">
<div class="contenttitle"> Our Associates</div>

<ul>
<li><a href="http://www.how2franchise.co.uk/" rel="nofollow"target="_blank"> <img src="<?php echo get_template_directory_uri(); ?>/images/ourassociate.png" alt="" height="93" /></a> </li>
<li><a href="http://www.nhancewebsolutions.com/" rel="nofollow" target="_blank"> <img src="<?php echo get_template_directory_uri(); ?>/images/ourassociate.1png.png" alt="" height="93" /></a> </li>
<li> <a href="http://www.tranterlowe.com/" rel="nofollow" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/ourassociate.2png.png" alt="" height="93" /></a> </li>
<li> <a href="http://www.britishchauffeursguild.co.uk/" rel="nofollow" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/ourassociate.3png.png" alt="" height="93" /> </a> </li>


</ul>
<!--<ul>
<?php
$images = miu_get_images($post_id=32);
foreach ($images as $image):
    ?>
  <li>   <img src="<?php echo $image; ?>" alt="" height="93" /> </li>
<?php endforeach; ?>
</ul>
-->
</div>

<div class="cl"></div>
At White7 (UK) Ltd, we don’t settle for the ordinary. We offer our customers a chauffeuring experience like no other. With our fleet of white BMWs and professionally trained drivers, White7 is the premier provider of chauffeur driven vehicles in the UK. <br>
<br>
Our clients are our biggest assets and we’re prepared to cater to their every need. <br>
<br>

Whether you’re looking at conveyance for a grand wedding or a reliable chauffeur car service for your business trip, we’ve got it all covered. Every time you choose our services, you’ll know exactly why we’re known to have the best chauffeur driven cars in the UK. Our chauffeurs aren’t just great drivers, they’re either serving or former police officers who know exactly how to handle any situation and assist you in every way possible. Once they get behind the wheel, you can sit back and indulge in the pure luxury of our cars. White7 vehicles are equipped with cutting-edge safety technology that you can only expect from a carmaker like BMW.  <br>
<br>

Our luxury fleet includes a BMW 7-Series long-wheelbase sedan, four BMW 5-Series M Sport Sedans, and a BMW Z4 hardtop convertible. However, we’re always prepared to provide our clients with additional vehicles upon request. The 7-Series LWB comes with its own built-in refrigerator, electronic blinds, and a complete rear seat entertainment package. 


</div>
</div>

<div class="footer">
<div class="mx_width">
  <div class="aboutwhite">
  <div class="fooertitle"><?php echo get_the_title(41); ?></div>
 
<div class="home-aboutcontent">
              <?php 

$page_id = 41;
$page_data = get_page( $page_id ); 
$content = apply_filters('the_content', $page_data->post_content); 
$title = $page_data->post_title;
 echo do_shortcode( $content );
?> 
</div>


  <div class="readmore"><a href="<?php echo get_page_link(41); ?>">Read more..</a></div></p>
  </div>
  <div class="awards">
  <div class="fooertitle">Awards</div>
  <ul>
 <?php
query_posts('cat=4&showposts=4'); 
while (have_posts()) : the_post();


//the_title();
{?>


<li> 
<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'twentytwelve' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
	</li>
            
    <?php }?>

  <?php       
endwhile;
?>
  <div class="cl"></div>
  </ul>

  </div>
    
  <div class="news">
  <div class="fooertitle">News</div>
  <ul>
  
  <?php
query_posts('cat=1&showposts=4'); 
while (have_posts()) : the_post();


//the_title();
{?>


<li> 
<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'twentytwelve' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
	</li>
            
    <?php }?>

  <?php       
endwhile;
?>
 
  <div class="cl"></div>
  </ul>

  </div>
  <div class="cl"></div>
 <div class="copy"><?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Copyright Text Number') ) : ?> <?php endif; ?></div>
 <div class="footerMenu"><a href="<?php echo esc_url( home_url( '/terms-conditions' ) ); ?>">Terms & Conditions</a>  </div>
</div>

</div>

<?php wp_footer(); ?>
<?php 
//get_sidebar();

//get_footer();

?>