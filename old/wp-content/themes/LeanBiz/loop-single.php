<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<div class="entry post clearfix<?php if ( get_option('leanbiz_show_postcomments') == 'false' || 0 == get_comments_number() ) echo ' comments_disabled'; ?>">
		<?php if (get_option('leanbiz_integration_single_top') <> '' && get_option('leanbiz_integrate_singletop_enable') == 'on') echo(get_option('leanbiz_integration_single_top')); ?>
		
		<?php if ( get_option('leanbiz_postinfo2') <> '' && ( in_array( 'date', get_option('leanbiz_postinfo2') ) || in_array( 'comments', get_option('leanbiz_postinfo2') ) ) ) { ?>
			
		<?php } ?>
		

		
		<h1 class="title"><?php the_title(); ?></h1>
		

	
		<?php the_content(); ?><br>
		<?php
			$thumb = '';
			$width = 500;
			$height = 350;
			$classtext = 'post-thumb';
			$titletext = get_the_title();
			$thumbnail = get_thumbnail($width,$height,$classtext,$titletext,$titletext,false,'Entry');
			$thumb = $thumbnail["thumb"];
		?>
		<?php 
			$category = get_the_category();
			if ( $thumb <> '' && get_option( 'leanbiz_thumbnails' ) == 'on' && $category[0]->cat_ID != 28 ) { ?>
			<div class="post-thumbnail">
				         <?php /*
                                         <a href="<?php the_permalink(); ?>">
					<?php print_thumbnail($thumb, $thumbnail["use_timthumb"], $titletext, $width, $height, $classtext); ?>  */ ?>
					<span class="post-overlay"></span>
				</a>
			</div> 	<!-- end .post-thumbnail -->
		<?php } ?>
		<?php wp_link_pages(array('before' => '<p><strong>'.esc_attr__('Pages','LeanBiz').':</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
		<?php edit_post_link(esc_attr__('Edit this page','LeanBiz')); ?>
	</div> <!-- end .entry -->

	<?php if (get_option('leanbiz_integration_single_bottom') <> '' && get_option('leanbiz_integrate_singlebottom_enable') == 'on') echo(get_option('leanbiz_integration_single_bottom')); ?>		
					
	<?php if (get_option('leanbiz_468_enable') == 'on') { ?>
		<?php 
			if(get_option('leanbiz_468_adsense') <> '') echo(get_option('leanbiz_468_adsense'));
			else { ?>
			   <a href="<?php echo esc_url(get_option('leanbiz_468_url')); ?>"><img src="<?php echo esc_url(get_option('leanbiz_468_image')); ?>" alt="468 ad" class="foursixeight" /></a>
	   <?php } ?>   
	<?php } ?>

	<?php if (get_option('leanbiz_show_postcomments') == 'on') comments_template('', true); ?>
<?php endwhile; // end of the loop. ?>