					</div> <!-- end .container -->
					<?php 
						global $et_show_featured_slider;
						if ( $et_show_featured_slider ){
							echo '<a id="left-arrow" href="#">' . esc_html__('Previous','LeanBiz') . '</a>';
							echo '<a id="right-arrow" href="#">' . esc_html__('Next','LeanBiz') . '</a>';
						}
					?>
				</div> <!-- end #featured-light -->	
			</div> <!-- end #featured-shadow -->	
		</div> <!-- end #featured -->

		
		<?php if ( is_home() ) { ?>

<div class="container" style="margin-bottom:10px;">
<h6><?php if (function_exists (ptmsshow)) ptmsshow(); ?></h6> 

</div>

<div id="header">
<div class="container">

<div style="text-align:center; background:white; padding-top:10px;; padding-bottom:10px; ">
<div style="display: inline-block;"> 
<h6 >Our Corporate Partners</h6></div>
<div style="display: inline-block; float:right; margin-right:35px; margin-top:-2px;"> 
 <h6>Our Charity for 2013</h6></div>
<div style = "text-align:center;">



<div style="display: inline-block; margin-right: 50px; margin-top:10px; margin-left:7px;">

<a href="http://www.rybrookshrewsburybmw.co.uk/" target="blank"><img src="http://www.white7.co.uk/wp-content/uploads/2012/12/bmw.png"></a> 
</div>

<div style="display: inline-block; margin-right: 50px; margin-top:10px;">

<a href="http://www.britishchauffeursguild.co.uk/" target="blank"><img src="http://www.white7.co.uk/wp-content/uploads/2012/12/nbs.png"> </a>
</div>




<div style="display: inline-block; margin-right:70px;">


 <a href="http://beostores.bang-olufsen.co.uk/shropshire-shrewsbury/welcome" target="blank"><img src="http://www.white7.co.uk/wp-content/uploads/2013/01/BO-Shrewsbury.jpg" ></a>
</div>

<div style="display: inline-block; position:relative; float:right;" > 

<a href="http://www.midlandsairambulance.com/" target="blank"><img src="http://www.white7.co.uk/wp-content/uploads/2012/12/mba.jpg"  alt="white7 corporate_partners"  /></a>
</div>

</div>
</div>
</div>
</div>
			<?php if ( get_option('leanbiz_blog_style') == 'on' ) { ?>
				<div class="container">
					<div id="content-border">
						<div id="content-top-border-shadow"></div>
						<div id="content-bottom-border-shadow"></div>
						<div id="content" class="clearfix">
                                                    
							<div id="content-right-bg" class="clearfix">
								<div id="left-area">

									<?php get_template_part('includes/entry','home'); ?>
								</div> 	<!-- end #left-area -->
								<div id="content-top-shadow"></div>
								<div id="content-bottom-shadow"></div>
								<div id="content-widget-light"></div>

								<?php get_sidebar(); ?>
							</div> <!-- end #content-right-bg -->	
						</div> <!-- end #content -->
					</div> <!-- end #content-border -->

				</div> <!-- end .container -->	

			<?php } else { ?>
				<div id="featured-border">
					<div id="featured-bottom-shadow"></div>
				</div> <!-- end #featured-border -->
			<?php } ?>
		<?php } ?>
			
		<div id="footer">
			<div class="container">
				<?php if ( is_active_sidebar('footer-area') ) { ?>
					<div id="footer-widgets" class="clearfix">
						<?php dynamic_sidebar('footer-area'); ?>
					</div> <!-- end #footer-widgets -->
				<?php } ?>
				<div id="footer-bottom">
                               		<p id="copyright">&copy; 2012 <?php echo esc_attr(get_bloginfo('name')); ?> Limited | Telephone: 01743 360007 | <?php esc_html_e('Designed by ','LeanBiz'); ?> <a href="http://www.matm.co.uk" title="matm">MATM</a></p>
				</div> <!-- end #footer-bottom -->
			</div> <!-- end .container -->
		</div> <!-- end #footer -->		
	</div> <!-- end #et-wrapper -->
	
	<?php wp_footer(); ?>
</body>
</html>