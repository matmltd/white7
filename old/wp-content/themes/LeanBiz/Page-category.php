<?php session_start(); 
/*
Template Name: Category Page
*/

php get_header(); ?>

<?php get_template_part('includes/breadcrumbs','index'); ?>

<div id="content-border">
	<div id="content-top-border-shadow"></div>
	<div id="content-bottom-border-shadow"></div>
	<div id="content" class="clearfix">
			
			<header class="page-header">
					<h1 class="page-title">
						<?php if ( is_day() ) : ?>
							<?php printf( __( 'Daily Archives: %s', 'twentyeleven' ), '<span>' . get_the_date() . '</span>' ); ?>
						<?php elseif ( is_month() ) : ?>
							<?php printf( __( 'Monthly Archives: %s', 'twentyeleven' ), '<span>' . get_the_date( _x( 'F Y', 'monthly archives date format', 'twentyeleven' ) ) . '</span>' ); ?>
						<?php elseif ( is_year() ) : ?>
							<?php printf( __( 'Yearly Archives: %s', 'twentyeleven' ), '<span>' . get_the_date( _x( 'Y', 'yearly archives date format', 'twentyeleven' ) ) . '</span>' ); ?>
						<?php else : ?>
							<?php _e( 'Blog Archives', 'twentyeleven' ); ?>
						<?php endif; ?>
					</h1>
				</header>
<?php
		// ok, let's use the wp functionality to pull in all child category data
		$children = get_categories( 'parent='. $current_cat );
		foreach($children as $child) {
?>
			<div class="subcat">
				<h3><a href="?cat=<?php echo $child->term_id; ?>"><?php echo $child->name; ?></a></h3>
				<!-- you might put an image, description, whatever here -->
			</div>
<?php
		}
?>
		</div>
<?php	
	// no results, show products (posts) in this category
	} else {
?>
		<div id="content">
		
		<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>
				<div class="post">
					<h2 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
					<p class="entry-meta">Posted on <?php the_time('l dS F, Y') ?> in <?php the_category( ', ' ); ?></p>
					
					<?php the_excerpt(''); ?>
				</div>
			<?php endwhile; ?>
			
	<?php
			if (function_exists('wp_paginate')) {
				wp_paginate();
			} else {
	?>
				<div id="page-selector">
					<p class="col-left"><?php next_posts_link('&laquo; Newer Entries') ?></p>
					<p class="col-right textright"><?php previous_posts_link('Older Entries &raquo;') ?></p>
				</div>
	<?php	} ?>

		<?php else : ?>

			<h1>Error</h1>
			<p>Archives could not be found.</p>

		<?php endif; ?>
		
		</div>
<div id="content-top-shadow"></div>
			<div id="content-bottom-shadow"></div>
			<div id="content-widget-light"></div>

			<?php get_sidebar(); ?>
		</div> <!-- end #content-right-bg -->	
	</div> <!-- end #content -->
</div> <!-- end #content-border -->	

<?php get_footer(); ?>